from setuptools import setup, find_packages

setup(
    name='iopener-finmath',
    version='0.1',  # Update the version number as needed
    packages=find_packages(),
    install_requires=[],  # Add any dependencies here
    python_requires='>=3.6',  # Specify the Python versions supported
    author='Víctor Villalobos',
    author_email='vpva.eien@gmail.com',
    description='A description of your library',
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    url='https://gitlab.com/iopener/finmath',  # Update with your repository URL
    license='GNU',
    classifiers=[
        'License :: OSI Approved :: GNU License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
    ],
)