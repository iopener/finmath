def income_growth_t0_t1(income_t0: float, income_t1: float) -> float:
    """
    Calculates the income growth rate from time period t0 to time period t1.

    Parameters:
        income_t0 (float): The income at time period t0 (Most recent period).
        income_t1 (float): The income at time period t1 (Previous period).

    Returns:
        float: The income growth rate from time period t0 to time period t1.

    Raises:
        ZeroDivisionError: If income_t1 is zero.
    """
    return income_t0 / income_t1