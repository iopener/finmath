# validar la documentación

def current_ratio(current_assets: float, current_liabilities: float) -> float:
    """
    Calculates the current ratio.

    The current ratio is a liquidity ratio that measures a company's ability to cover its short-term
    obligations with its short-term assets.

    Parameters:
        current_assets (float): The total current assets of the company.
        current_liabilities (float): The total current liabilities of the company.

    Returns:
        float: The current ratio.

    Raises:
        ZeroDivisionError: If current_liabilities is zero.
    """
    return current_assets / current_liabilities

def liquid_assets(cash_and_cash_equivalents: float, accounts_receivable: float) -> float:
    """
    Calculates the liquid assets of a company.

    Liquid assets are assets that can be quickly converted into cash without losing value.

    Parameters:
        cash_and_cash_equivalents (float): The total cash and cash equivalents of the company.
        accounts_receivable (float): The total accounts receivable of the company.

    Returns:
        float: The liquid assets of the company.
    """
    return cash_and_cash_equivalents + accounts_receivable

def quick_ratio(liquid_assets: float, current_liabilities: float) -> float:
    """
    Calculates the quick ratio.

    The quick ratio, also known as the acid-test ratio, is a liquidity ratio that measures a company's
    ability to cover its short-term obligations with its most liquid assets.

    Parameters:
        liquid_assets (float): The total liquid assets of the company.
        current_liabilities (float): The total current liabilities of the company.

    Returns:
        float: The quick ratio.

    Raises:
        ZeroDivisionError: If current_liabilities is zero.
    """
    return liquid_assets / current_liabilities

def cash_ratio(cash_and_cash_equivalents: float, current_liabilities: float) -> float:
    """
    Calculates the cash ratio.

    The cash ratio is a liquidity ratio that measures a company's ability to cover its short-term
    obligations with its cash and cash equivalents.

    Parameters:
        cash_and_cash_equivalents (float): The total cash and cash equivalents of the company.
        current_liabilities (float): The total current liabilities of the company.

    Returns:
        float: The cash ratio.

    Raises:
        ZeroDivisionError: If current_liabilities is zero.
    """
    return cash_and_cash_equivalents / current_liabilities

def expenditures(cost_of_sales_and_goods: float, selling_general_admin_expenses: float) -> float:
    """
    Calculates the total expenditures of a company.

    Total expenditures are the sum of the cost of sales and goods and the selling, general, and administrative expenses.

    Parameters:
        cost_of_sales_and_goods (float): The total cost of sales and goods of the company.
        selling_general_admin_expenses (float): The total selling, general, and administrative expenses of the company.

    Returns:
        float: The total expenditures of the company.
    """
    return cost_of_sales_and_goods + selling_general_admin_expenses

def average_daily_expenditures(expenditures: float, days: float) -> float:
    """
    Calculates the average daily expenditures of a company.

    The average daily expenditures are the total expenditures divided by the number of days.

    Parameters:
        expenditures (float): The total expenditures of the company.
        days (int): The number of days.

    Returns:
        float: The average daily expenditures of the company.
    """
    return expenditures / days

def defensive_interval(liquid_assets: float, average_daily_expenditures: float) -> float:
    """
    Calculates the defensive interval.

    The defensive interval is a liquidity ratio that measures the number of days a company can operate using
    its liquid assets without additional cash inflows.

    Parameters:
        liquid_assets (float): The total liquid assets of the company.
        average_daily_expenditures (float): The average daily expenditures of the company.

    Returns:
        float: The defensive interval.

    Raises:
        ZeroDivisionError: If average_daily_expenditures is zero.
    """
    return liquid_assets / average_daily_expenditures

def cash_conversion_cycle(days_sales_outstanding: float, days_inventory_on_hand: float) -> float:
    """
    Calculates the cash conversion cycle.

    The cash conversion cycle is a metric that measures the time it takes for a company to convert its
    investments in inventory and accounts receivable into cash flows from sales.

    Parameters:
        days_sales_outstanding (float): The number of days sales outstanding.
        days_inventory_on_hand (float): The number of days inventory is held on hand.

    Returns:
        float: The cash conversion cycle.
    """
    return days_sales_outstanding + days_inventory_on_hand