current_financial_liabilities
non_current_financial_liabilities
total_debt
equity
total_debt_to_equity
capital
debt_to_capital
debt_to_assets
financial_leverage
ebit
interest_payments
interest_coverage

def total_debt(current_financial_liabilities: float, non_current_financial_liabilities: float) -> float:
    """
    Calculates the total debt of a company.

    Total debt is the sum of a company's current financial liabilities and non-current financial liabilities.

    Parameters:
        current_financial_assets (float): The total value of current financial assets.
        non_current_financial_assets (float): The total value of non-current financial assets.

    Returns:
        float: The total debt of the company.
    """
    return current_financial_liabilities + non_current_financial_liabilities

def total_debt_to_equity(total_debt: float, equity: float) -> float:
    """
    Calculates the total debt to equity ratio.

    The total debt to equity ratio measures the proportion of a company's total debt to its total equity.

    Parameters:
        total_debt (float): The total debt of the company.
        equity (float): The total equity of the company.

    Returns:
        float: The total debt to equity ratio.

    Raises:
        ZeroDivisionError: If equity is zero.
    """
    return total_debt / equity

def capital(equity: float, total_debt: float) -> float:
    """
    Calculates the capital of a company.

    Capital is the sum of a company's equity and total debt.

    Parameters:
        equity (float): The total equity of the company.
        total_debt (float): The total debt of the company.

    Returns:
        float: The capital of the company.
    """
    return equity + total_debt

def debt_to_capital(total_debt: float, capital: float) -> float:
    """
    Calculates the debt to capital ratio.

    The debt to capital ratio measures the proportion of a company's total debt to its total capital.

    Parameters:
        total_debt (float): The total debt of the company.
        capital (float): The total capital of the company.

    Returns:
        float: The debt to capital ratio.

    Raises:
        ZeroDivisionError: If capital is zero.
    """
    return total_debt / capital

def debt_to_assets(total_debt: float, total_assets: float) -> float:
    """
    Calculates the debt to assets ratio.

    The debt to assets ratio measures the proportion of a company's total debt to its total assets.

    Parameters:
        total_debt (float): The total debt of the company.
        total_assets (float): The total assets of the company.

    Returns:
        float: The debt to assets ratio.

    Raises:
        ZeroDivisionError: If total assets is zero.
    """
    return total_debt / total_assets

def financial_leverage(total_assets: float, equity: float) -> float:
    """
    Calculates the financial leverage of a company.

    Financial leverage is a measure of the degree to which a company uses debt to finance its operations.

    Parameters:
        total_assets (float): The total assets of the company.
        equity (float): The total equity of the company.

    Returns:
        float: The financial leverage of the company.

    Raises:
        ZeroDivisionError: If equity is zero.
    """
    return total_assets / equity

def interest_coverage(ebit: float, interest_payments: float) -> float:
    """
    Calculates the interest coverage ratio.

    The interest coverage ratio measures a company's ability to meet its interest payments on its debt.

    Parameters:
        ebit (float): The earnings before interest and taxes.
        interest_payments (float): The total interest payments.

    Returns:
        float: The interest coverage ratio.

    Raises:
        ZeroDivisionError: If interest payments is zero.
    """
    return ebit / interest_payments