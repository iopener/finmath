### Review if documentation its correct

def costs_of_sales_and_goods_to_revenue(cost_of_sales_and_goods: float, revenue: float) -> float:
    """
    Calculates the ratio of costs of sales and goods to revenue.

    Parameters:
        cost_of_sales_and_goods (float): The total cost of sales and goods.
        revenue (float): The total revenue.

    Returns:
        float: The ratio of costs of sales and goods to revenue.

    Raises:
        ZeroDivisionError: If revenue is zero.
    """
    return cost_of_sales_and_goods / revenue

def gross_profit(revenue: float, cost_of_sales_and_goods: float) -> float:
    """
    Calculates the gross profit by subtracting the cost of sales and goods from the revenue.

    Parameters:
        revenue (float): The total revenue generated.
        cost_of_sales_and_goods (float): The cost of sales and goods.

    Returns:
        float: The gross profit.
    """
    return revenue - cost_of_sales_and_goods

def gross_margin(gross_profit: float, revenue: float) -> float:
    """
    Calculate the gross margin ratio.

    The gross margin ratio is calculated by dividing the gross profit by the revenue.

    Parameters:
        gross_profit (float): The gross profit.
        revenue (float): The revenue.

    Returns:
        float: The gross margin ratio.
    
    Raises:
        ZeroDivisionError: If revenue is zero.
    """
    return gross_profit / revenue

def gross_margin_(revenue: float, cost_of_sales_and_goods: float) -> float:
    """
    Calculate the gross margin ratio.

    The gross margin ratio is calculated by dividing the gross profit by the revenue.

    Parameters:
        revenue (float): The total revenue.
        cost_of_sales_and_goods (float): The cost of sales and goods.

    Returns:
        float: The gross margin ratio.

    Raises:
        ZeroDivisionError: If revenue is zero.
    """
    return gross_margin(gross_profit(revenue, cost_of_sales_and_goods), revenue)

def selling_general_admin_expenses_margin(selling_general_admin_expenses: float, revenue: float) -> float:
    """
    Calculates the selling, general, and administrative expenses margin.

    Parameters:
        selling_general_admin_expenses (float): The total selling, general, and administrative expenses.
        revenue (float): The total revenue.

    Returns:
        float: The selling, general, and administrative expenses margin.

    Raises:
        ZeroDivisionError: If revenue is zero.
    """
    return selling_general_admin_expenses / revenue

def other_operating_expenses_margin(other_operating_expenses: float, revenue: float) -> float:
    """
    Calculates the other operating expenses margin.

    Parameters:
        other_operating_expenses (float): The total amount of other operating expenses.
        revenue (float): The total revenue.

    Returns:
        float: The other operating expenses margin, expressed as a percentage.

    Raises:
        ZeroDivisionError: If revenue is zero.
    """
    return other_operating_expenses / revenue

def other_operating_income_margin(other_operating_income: float, revenue: float) -> float:
    """
    Calculates the other operating income margin.

    Parameters:
        other_operating_income (float): The amount of other operating income.
        revenue (float): The total revenue.

    Returns:
        float: The other operating income margin.

    Raises:
        ZeroDivisionError: If revenue is zero.
    """
    return other_operating_income / revenue

def ebitda(gross_profit: float, selling_general_admin_expenses: float) -> float:
    """
    Calculates the EBITDA (Earnings Before Interest, Taxes, Depreciation, and Amortization) 
    by subtracting the selling, general, and administrative expenses from the gross profit.

    Parameters:
        gross_profit (float): The gross profit of the company.
        selling_general_admin_expenses (float): The selling, general, and administrative expenses of the company.

    Returns:
        float: The calculated EBITDA.
    """
    return gross_profit - selling_general_admin_expenses

def ebitda_(revenue: float, cost_of_sales_and_goods: float, selling_general_admin_expenses: float) -> float:
    """
    Calculates the EBITDA (Earnings Before Interest, Taxes, Depreciation, and Amortization) using the given inputs.

    Parameters:
        revenue (float): The total revenue.
        cost_of_sales_and_goods (float): The cost of sales and goods.
        selling_general_admin_expenses (float): The selling, general, and administrative expenses.

    Returns:
        float: The calculated EBITDA.
    """
    return gross_profit(revenue, cost_of_sales_and_goods) - selling_general_admin_expenses

def ebitda_margin(ebitda: float, revenue: float) -> float:
    """
    Calculates the EBITDA margin.

    The EBITDA margin is a financial metric that measures the profitability of a company by
    determining the percentage of revenue that is converted into EBITDA (Earnings Before Interest,
    Taxes, Depreciation, and Amortization).

    Parameters:
        ebitda (float): The EBITDA value.
        revenue (float): The revenue value.

    Returns:
        float: The EBITDA margin as a percentage.

    Raises:
        ZeroDivisionError: If the revenue is zero.
    """
    return ebitda / revenue

def ebitda_margin_(revenue: float, cost_of_sales_and_goods: float, selling_general_admin_expenses: float) -> float:
    """
    Calculates the EBITDA margin given the revenue, cost of sales and goods, and selling general and administrative expenses.

    Parameters:
        revenue (float): The total revenue.
        cost_of_sales_and_goods (float): The cost of sales and goods.
        selling_general_admin_expenses (float): The selling, general, and administrative expenses.

    Returns:
        float: The EBITDA margin.
    """
    return ebitda(gross_profit(revenue, cost_of_sales_and_goods), selling_general_admin_expenses)

def ebit(ebitda: float, depreciation_and_amortization: float) -> float:
    """
    Calculates the Earnings Before Interest and Taxes (EBIT) by subtracting
    the depreciation and amortization from the Earnings Before Interest, Taxes,
    Depreciation, and Amortization (EBITDA).

    Parameters:
        ebitda (float): The Earnings Before Interest, Taxes, Depreciation, and Amortization.
        depreciation_and_amortization (float): The depreciation and amortization.

    Returns:
        float: The Earnings Before Interest and Taxes (EBIT).
    """
    return ebitda - depreciation_and_amortization

def ebit_(revenue: float, cost_of_sales_and_goods: float, selling_general_admin_expenses: float, depreciation_and_amortization: float) -> float:
    """
    Calculates the Earnings Before Interest and Taxes (EBIT) given the revenue, cost of sales and goods, selling general and administrative expenses, and depreciation and amortization.

    Parameters:
        revenue (float): The total revenue.
        cost_of_sales_and_goods (float): The cost of sales and goods.
        selling_general_admin_expenses (float): The selling, general, and administrative expenses.
        depreciation_and_amortization (float): The depreciation and amortization expenses.

    Returns:
        float: The Earnings Before Interest and Taxes (EBIT).
    """
    return ebitda_(revenue, cost_of_sales_and_goods, selling_general_admin_expenses) - depreciation_and_amortization

def effective_tax_rate(ebit: float, tax_expense: float) -> float:
    """
    Calculates the effective tax rate.

    Parameters:
        ebit (float): The earnings before interest and taxes.
        tax_expense (float): The total tax expense.

    Returns:
        float: The effective tax rate.

    Raises:
        ZeroDivisionError: If EBIT is zero.
    """
    return tax_expense / ebit

def effective_tax_rate_(revenue: float, cost_of_sales_and_goods: float, selling_general_admin_expenses: float, depreciation_and_amortization: float, tax_expense: float) -> float:
    """
    Calculates the effective tax rate.

    Parameters:
        revenue (float): The revenue of the company.
        cost_of_sales_and_goods (float): The cost of sales and goods.
        selling_general_admin_expenses (float): The selling, general, and administrative expenses.
        depreciation_and_amortization (float): The depreciation and amortization expenses.
        tax_expense (float): The tax expense.

    Returns:
        float: The effective tax rate.

    Raises:
        ZeroDivisionError: If EBIT is zero.
    """
    return tax_expense / ebit_(revenue, cost_of_sales_and_goods, selling_general_admin_expenses, depreciation_and_amortization)

def net_profit_margin(net_profit: float, revenue: float) -> float:
    """
    Calculates the net profit margin, which is the ratio of net profit to revenue.

    Parameters:
        net_profit (float): The net profit of a company.
        revenue (float): The revenue generated by a company.

    Returns:
        float: The net profit margin.
    
    Raises:
        ZeroDivisionError: If revenue is zero.
    """
    return net_profit / revenue

