def cash_and_cash_equivalents_over_total_assets(cash_and_cash_equivalents: float, total_assets: float) -> float:
    """
    Calculate the ratio of cash and cash equivalents over total assets.

    Parameters:
        cash_and_cash_equivalents (float): The cash and cash equivalents.
        total_assets (float): The total assets.

    Returns:
        float: The ratio of cash and cash equivalents over total assets.

    Raises:
        ZeroDivisionError: If total_assets is zero.
    """
    return cash_and_cash_equivalents / total_assets

def accounts_receivables_over_total_assets(accounts_receivables: float, total_assets: float) -> float:
    """
    Calculate the ratio of accounts receivables over total assets.

    Parameters:
        accounts_receivables (float): The accounts receivables.
        total_assets (float): The total assets.

    Returns:
        float: The ratio of accounts receivables over total assets.

    Raises:
        ZeroDivisionError: If total_assets is zero.
    """
    return accounts_receivables / total_assets

def inventory_over_total_assets(inventory: float, total_assets: float) -> float:
    """
    Calculate the ratio of inventory over total assets.

    Parameters:
        inventory (float): The inventory.
        total_assets (float): The total assets.

    Returns:
        float: The ratio of inventory over total assets.

    Raises:
        ZeroDivisionError: If total_assets is zero.
    """
    return inventory / total_assets

def other_current_assets_over_total_assets(other_current_assets: float, total_assets: float) -> float:
    """
    Calculate the ratio of other current assets over total assets.

    Parameters:
        other_current_assets (float): The other current assets.
        total_assets (float): The total assets.

    Returns:
        float: The ratio of other current assets over total assets.

    Raises:
        ZeroDivisionError: If total_assets is zero.
    """
    return other_current_assets / total_assets

def pp_and_e_over_total_assets(pp_and_e: float, total_assets: float) -> float:
    """
    Calculate the ratio of property, plant, and equipment (PP&E) over total assets.

    Parameters:
        pp_and_e (float): The property, plant, and equipment (PP&E).
        total_assets (float): The total assets.

    Returns:
        float: The ratio of PP&E over total assets.

    Raises:
        ZeroDivisionError: If total_assets is zero.
    """
    return pp_and_e / total_assets

def other_non_current_assets_over_total_assets(other_non_current_assets: float, total_assets: float) -> float:
    """
    Calculate the ratio of other non-current assets over total assets.

    Parameters:
        other_non_current_assets (float): The other non-current assets.
        total_assets (float): The total assets.

    Returns:
        float: The ratio of other non-current assets over total assets.

    Raises:
        ZeroDivisionError: If total_assets is zero.
    """
    return other_non_current_assets / total_assets

def accounts_payable_over_total_assets(accounts_payable: float, total_assets: float) -> float:
    """
    Calculate the ratio of accounts payable over total assets.

    Parameters:
        accounts_payable (float): The accounts payable.
        total_assets (float): The total assets.

    Returns:
        float: The ratio of accounts payable over total assets.

    Raises:
        ZeroDivisionError: If total_assets is zero.
    """
    return accounts_payable / total_assets

def short_term_debt_over_total_assets(short_term_debt: float, total_assets: float) -> float:
    """
    Calculate the ratio of short-term debt over total assets.

    Parameters:
        short_term_debt (float): The short-term debt.
        total_assets (float): The total assets.

    Returns:
        float: The ratio of short-term debt over total assets.

    Raises:
        ZeroDivisionError: If total_assets is zero.
    """
    return short_term_debt / total_assets

def other_current_liabilities_over_total_assets(other_current_liabilities: float, total_assets: float) -> float:
    """
    Calculates the ratio of other current liabilities to total assets.

    Parameters:
        other_current_liabilities (float): The value of other current liabilities.
        total_assets (float): The value of total assets.

    Returns:
        float: The ratio of other current liabilities to total assets.

    Raises:
        ZeroDivisionError: If total_assets is zero.
    """
    return other_current_liabilities / total_assets

def long_term_debt_over_total_assets(long_term_debt: float, total_assets: float) -> float:
    """
    Calculates the ratio of long-term debt to total assets.

    Parameters:
        long_term_debt (float): The amount of long-term debt.
        total_assets (float): The total assets.

    Returns:
        float: The ratio of long-term debt to total assets.

    Raises:
        ZeroDivisionError: If total_assets is zero.
    """
    return long_term_debt / total_assets

def other_non_current_liabilities_over_total_assets(other_non_current_liabilities: float, total_assets: float) -> float:
    """
    Calculates the ratio of other non-current liabilities to total assets.

    Parameters:
        other_non_current_liabilities (float): The value of other non-current liabilities.
        total_assets (float): The value of total assets.

    Returns:
        float: The ratio of other non-current liabilities to total assets.

    Raises:
        ZeroDivisionError: If total_assets is zero.
    """
    return other_non_current_liabilities / total_assets

def equity_over_total_assets(equity: float, total_assets: float) -> float:
    """
    Calculates the equity over total assets ratio.

    Parameters:
        equity (float): The equity value.
        total_assets (float): The total assets value.

    Returns:
        float: The equity over total assets ratio.

    Raises:
        ZeroDivisionError: If total_assets is zero.
    """
    return equity / total_assets