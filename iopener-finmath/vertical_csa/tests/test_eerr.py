from .. import eerr
import pytest

class TestEERR:
            
    def test_ebitda(self):
        assert eerr.ebitda(100, 50) == 50
        assert eerr.ebitda(-80, 30) == -110

    def test_margen_bruto(self):
        assert eerr.margen_bruto(100, 10) == 10
        with pytest.raises(Exception):
            eerr.margen_bruto(100, 0)